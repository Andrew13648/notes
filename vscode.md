**Useful Settings**

Go to File -> Preferences -> Settings or press <kbd>Ctrl</kbd> + <kbd>,</kbd> and click the icon on the right to open the `settings.json` file.

* `"workbench.tree.indent": 25`
* `"editor.formatOnSave": true`
* `"editor.rulers": [ 80 ]` adds a print margin or ruler to the editor

**How to set up tasks to execute:**

Go to View -> Command Palette or <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>P</kbd> and Tasks: Configure Tasks.

Otherwise, just open the `/.vscode/tasks.json` file that defines tasks that may look like this:
```json
{
    "version": "2.0.0",
    "tasks": [
        {
            "label": "Chrome",
            "type": "process",
            "command": "chrome.exe",
            "windows": {
                "command": "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
            },
            "args": [
                // "--allow-file-access-from-files",
                // "-incognito",
                "http://localhost:8080/${relativeFile}",
            ],
            "problemMatcher": []
        },
        {
            "label": "echo",
            "type": "shell",
            "command": "echo hello"            
        },
        {
            "label": "mocha",
            "type": "shell",
            "command": "./node_modules/.bin/mocha",
            "args": [
                "\"${relativeFile}\""
            ]
        }
    ]
}
```

**How to define keyboard shortcuts to run tasks:**

1.  Go to File -> Preferences -> Keyboard Shortcuts
2.  On the right, there is a small icon that says `Open Keyboard Shortcuts (JSON)`
3.  Clicking that will open the file at `/%userprofile%/AppData/Roaming/Code/User/keybindings.json`, which will look like this:

```json
[
    {
        "key": "ctrl+shift+f9",
        "command": "workbench.action.tasks.runTask",
        "args": "echo"
    },
    {
        "key": "ctrl+shift+f10",
        "command": "workbench.action.tasks.runTask",
        "args": "Chrome"
    },
    {
        "key": "ctrl+shift+f11",
        "command": "workbench.action.tasks.runTask",
        "args": "mocha"
    }
]
```

**Why does `ll` work in Git Bash but not in the Bash terminal in VSCode?**

When Git Bash starts, it reads and executes commands from `C:\Program Files\Git\etc\profile.d`, which includes an `aliases.sh` file that sets `alias ll='ls -l'`.  Bash within VSCode reads and executes only `~/.bashrc` (or `~/.bash_profile` if a login shell is being used).  You can specify a login shell by setting `"terminal.integrated.shellArgs.windows": ["-l"]` within the VSCode `settings.json` file (`C:\Users\Andrew\AppData\Roaming\Code\User\settings.json`).

