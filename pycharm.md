**Set the PyCharm Terminal to Git Bash**

Go to File -> Settings -> Tools -> Terminal and set the Shell Path to `"C:\Program Files\Git\bin\sh.exe" --login`.

**Set Line Margins**

Go to File -> Settings -> Editor -> Code Style and add Visual guides: 80, 100

**Reformat a File**

<kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>L<kbd>